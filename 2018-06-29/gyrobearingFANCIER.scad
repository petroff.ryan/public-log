// Sphere Gyro Bearings for Fidgets
    // Ryan Petroff's design

    // Designed to enable more wacky zero-assembly 3DP fidgets
    // Confirmed fun
    // Designed to function as a drop-in replacement for metal bearings (skateboard bearings) {22mm diameter, 7mm thick}

    // All measurements in mm milimeters 


//////////////////////////////////////////////
/////////////////////////////////////////////
//   USER EDITABLE SECTION   //////
///////////////////////////////////////////
//////////////////////////////////////////
    
    // Width of extruder nozzle
NOZZLEwidth = 0.4 ;
    
/////////////////////////////////////////

    // Minimum gap size on printer (discovered empirically)
G = 0.35 ;
    // The smallest open slit the printer can make.
    // Find this G for your printer by printing a gap test calibration print.  
    // Find one here https://www.thingiverse.com/thing:42623 or maybe find script/stl included in this project
    
/////////////////////////////////////////

    // Resolution
$fn=400;
    // Comment out for development
    // Enable with a high value for final export

////////////////////////////////////////
///////////////////////////////////////
//////////////////////////////////////


    // Height of whole print
H = 7 ; 

RADIUSouterCYLINDER = (22/2) ;

RADIUSsphereVOID = RADIUSouterCYLINDER - (2*NOZZLEwidth); 
    // this might need to be 3*NOZZLEwidth - experiment!

RADIUSsphereSOLID = RADIUSsphereVOID - G;


////////// MATH ///////////
// RADIUSsphereVOID = RADIUSouterCYLINDER - (2*NOZZLEwidth) - FITadjustment;

// we must compare the diameter of the circular hole on the side to the diameter of the inner sphere in order to find the necessary fit adjustment

// Using the Pythagorean Theorem
SIDEHOLEradius = sqrt ( pow( RADIUSsphereVOID , 2) - pow( (H/2) , 2) ) ;

CAPTIVEmargin = RADIUSsphereSOLID - SIDEHOLEradius ;
echo("captive margin is ", CAPTIVEmargin);
//difference () {
//    cylinder ( r=RADIUSsphereSOLID, h=H) ;
//    cylinder ( r=SIDEHOLEradius, h=H) ;
//}

// if captive margin is too small, center part will pop out
// How little is too litte? Experiment!
// if CAPTIVEmargin is greater than NOZZLEwidth, it's probably really safe

// This script could potentially subtract from RADIUSsphereVOID until CAPTIVEmargin is greater than NOZZLEwidth, or greater than some experimentally derived value

///////////////////////////////
// This next bit of code adopted from https://www.myminifactory.com/object/3d-print-tools-for-fillets-and-chamfers-on-edges-and-corners-straight-and-or-round-45862
// filletr round edge

// rf = radius of fillet
// re = radius of edge
// i= insideout 1 = outside, -1 = inside

// What???
// original value -> // az=0.001; //anti z fight
az=0.00001 ;
// ?????????

module filletr(rf,re,i){
	translate([0,0,-rf+az]){
		difference(){
			union(){
			//echo(rf);
    rotate_extrude(angle=360){
     translate([re*i-rf,0,0]){ 
				 	difference(){
				 	 rotate ([0,0,180]) translate([-rf-az,-rf-az,0]) square([rf+az,rf+az]);
				 		circle(rf);
					 } //end difference	
					} //end translate
			 } //end extrude	
			} //end union
		} //end difference
	} //end translate	
} //end module


////////////////////

// Outer race
difference () {
    cylinder ( r=RADIUSouterCYLINDER, h=H, center=true );
    sphere (RADIUSsphereVOID, center=true);
} ;

// Inner race
difference () {
    sphere (RADIUSsphereSOLID, center=true);
    translate([-50,-50,(H/2)])
        cube ([100,100,100]);
    translate([-50,-50,((-100)+(-H/2))])
        cube ([100,100,100]);
    translate([0,0,-H])
        cylinder ( r=(8/2), h=(2*H) ) ;
    //translate([0,0,(-H)])
    
    // Center hole grips
    GRIPsphereRADIUS = SIDEHOLEradius*8/11;
    GRIPsphereDISTANCE = H*1/2;
    translate([0,0,(GRIPsphereDISTANCE)]) sphere(GRIPsphereRADIUS, center=true) ;
    translate([0,0,(-GRIPsphereDISTANCE)]) sphere(GRIPsphereRADIUS, center=true) ;
    // Center hole fillets for sphere hole grips
    GRIPsphereEDGEradius = sqrt ( pow( GRIPsphereRADIUS , 2) - pow( (GRIPsphereDISTANCE - (H/2)) , 2) ) ;
    translate([0,0,(H/2)]) filletr(0.5,GRIPsphereRADIUS-0.1, -1);
    translate([0,0,(-H/2)]) rotate ([0,180,0]) filletr(0.5, GRIPsphereRADIUS-0.1, -1);
    
    // If this fillet cuts into the grip, move the sphere closer to the surface for steeper walls
    
    // Center hole fillets (maybe messy to print, overhang)(alternative to sphere center hole grips)
    // translate([0,0,(H/2)]) filletr(3.5,3.8, -1);
    // translate([0,0,(-H/2)]) rotate ([0,180,0]) filletr(3.5,3.8, -1);
    
    // sphere edge fillets
    translate([0,0,(H/2)]) filletr(0.7,SIDEHOLEradius,1);
    translate([0,0,(-H/2)]) rotate ([0,180,0]) filletr(0.7,SIDEHOLEradius,1);
}
//translate([0,0,(H/2)]) filletr(0.4,SIDEHOLEradius,1);
