// Sphere Gyro Bearings for Fidgets
    // Ryan Petroff's design

    // Designed to enable more wacky zero-assembly 3DP fidgets
    // Confirmed fun and functional
    // Designed to function as a drop-in replacement for metal bearings {22mm diameter, 7mm thick}

    // All measurements in mm milimeters 


///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////

    // Minimum gap size on printer (discovered empirically)
G = 0.35 ;
    // Find this G for your printer
    // by printing a gap test calibration print
    // https://www.thingiverse.com/thing:42623
    // or find script/stl included in this project
    
    // Width of extruder nozzle
NOZZLEwidth = 0.4 ;
    
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


    // Height of whole print
H = 7 ; 

RADIUSouterCYLINDER = (22/2) ;

RADIUSsphereVOID = RADIUSouterCYLINDER - G - 1; 
// this -1 is experimental - more math required to bring it as low as possible as a function of G - should be 2*nozzlewidth or greater

RADIUSsphereSOLID = RADIUSsphereVOID - G;


// Outer race
difference () {
    cylinder ( r=RADIUSouterCYLINDER, h=H, $fn=200, center=true );
    sphere (RADIUSsphereVOID, center=true, $fn=200);
} ;

// Inner race
difference () {
    sphere (RADIUSsphereSOLID, center=true, $fn=200);
    translate([-50,-50,(H/2)])
        cube ([100,100,100]);
    translate([-50,-50,((-100)+(-H/2))])
        cube ([100,100,100]);
    translate([0,0,-H])
        cylinder ( r=(8/2), h=(2*H), $fn=200 ) ;
}