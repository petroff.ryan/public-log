# Towards a Toronto Freedom School

Sudbury Valley School Model plus Libre Technology - Freedom Maker School basically


## Immediate term
 - Create good plan:
    - Consult with smart people to improve plan
    - Read the plans of people who succeeded at similar goals

## Short Term
  - Create the systems necessary to execute the rest of the plan efficiently
    - Build a team and systems to grow that team

## Medium Term
  - build large collection of parents and young families on board

## Long Term
 - Found the Freedom School in Toronto

## Extended Long Term
 - Freedom schools in every cultural capital in the world
 - All students freed, all rights respected
 - Better democratic leadership for the free world
 - Reduction of human suffering, extreme reduction of systemic human suffering

### Marketing ideas
  - innovation outputs will be tremendous, all about the public good as well
  - Leadership!
  - Stories of Justice, the image of the 7 year old chair calling the staff out of order
      "I was visiting a school meeting, and the staff I had corresponded with were speaking when they were called 'Out of order!' with the banging of a gavel by the Chair.  They silenced themselves, and the 7 year old Chair continued her original point of business. Where else does a student chastise adult staff and receive a good result?"

      "I was graciously allowed to spectate a hearing of the Judiciary Committee. I have been in trouble many times, and I have been forced to act as an authority putting others in trouble many times, and in general am very familiar with institutional attitudes towards 'infractions' - the outcomes are rarely good. What I saw there was so shocking and so cathartic to witness that it was one of my strongest experiences - certainly one of the most positively emotional experiences of my life.
        So, I'm in the meeting of the Judiciary Committee, and a boy is brought up for threatening and intimidation against some other students. At length, the story comes out. Although the accused is clearly nervous and halting in his testimony, he is given ample time and patience to get his side across. Witnesses are summoned from the rest of the school grounds, and gradually, the whole story comes out. There are some heated moments, but cool heads are kept and everyone is allowed to be heard. Once the whole story had been revealed, it became clear that the accused had been more the victim than the antagonist. It was determined that the whole group of kids had been playing a game that violated the school's strict safeword policy. (No means no, no games where no means not no.) The 'sentence' of '3 days of no rough play' was accepted unanimously by everyone present, including the accused, and no sooner had it been accepted than the staff assisted this boy in writing up the other students who had participated. I saw /justice/ for that student, and it was like nothing I had ever seen before. It demostrates that justice is /possible/."


## Necessary Plan phases
### Planning
### Team building
### Formalizing
### Marketing
### Upkeep

## Resources
 - There must be some recognition that the key resource devoted to this plan is Ryan
 - Humans are the key throughout the plan - relationship management SUPER KEY
      Consider relationship management software?
 - It might make sense for the Ryan Plan to be incorporated here by reference

# Ryan Plan
  The plan for the correct maintenance and use of the Freedom School's key asset, Ryan.
  Ryan's Main Goal is the founding of a Freedom School in Toronto.
  Ryan would also like to have a happy poly home, finally able to know kids could be free.

  !!!!!!!!!
  !! Become the right person to found the Toronto School
  !!!!!!!!!

  ## Dailies
   - Affirming goals, reviewing plans
   - Eating
   - Exercising
   - Correspondences
   - Writing
   - Working the plan

  ## Immediate term
   - Create good plan:
      - Consult with smart people to improve plan
      - Read the plans of people who succeeded at similar goals

  ## Short Term
    - Reach out to friends, family, allies and request advice
    - Reach out to Freedom Schools systematically
    - eliminate stimulants (coffee) from lifestyle
      - use caffeine pills as replacement and reduction therapy

  ## Medium Term
    - staff at global Freedom schools, potentially as 3D imagineering instructor

  ## Long Term
   - Found the Freedom School in Toronto

  ## Extended Long Term
    - Live with my partners in a happy home full of love with a nice workshop and happy kids

  ## Writing and Publishing, Editing

  ## Poly community building

  ## Physical Upkeep

  ## Emotional Upkeep

  ## Intellectual Upkeep

  ## Spiritual Upkeep
    - design i3 enclosure and carrying case for portable good times

  ## Financial Upkeep









####
