Anoooother daaaaay!
I really need to ditch this coffee habit again.
I'm making progress, so I just need to stick with it, slowly lowering my stimulant intake to zero.

A tasty breakfast of lox and hempseed/flaxseed oil coffee behind me,
I consult my daily planner and find I must 1)do laundry, and 2) buy groceries.
As I write this, my clothes are in the washing machine in the basement of this apartment building.
I'll go out and fetch some eggs before I need to switch it to the dryer.

I read about the new hotness in Python https://hackaday.com/2018/07/23/hands-on-with-python-3-7-whats-new-in-the-latest-release/ and it's good and right. Feels good to be current, so next time I'm reading Python I won't get confused. :)

I'm keeping all my rules about my place pretty well,
and that means I have to keep all my other rooms in their 'clear' state before opening the
door to the lab.
Right now, my bed is unmade and there are papers on the main table, so the lab remains closed.
I'll re-set things here after I get back with the eggs.
I try to go a bit further with Python, in expectation of a Machine Vision jam with some friends of mine, but "PyPI is down for maintenance or is having an outage." it looks like I'll leave it for today.
This made me yell at my computer "Nooo, it works?!" https://hackaday.io/project/159350/gallery#35f7dbd3a9acc628015c15000b206428
So that's cool.

![existentialsafety.jpg](./2018-07-23/existentialsafety.jpg) existentialsafety.jpg cropped from Oglaf

----

Got the eggs, switched the laundry, and found my counter covered in coffee liquid. Why?
Science giveth, and science taketh away. My desire for ice-cold bulletproof coffee in the morning
has now destroyed two metal drinking flasks (the expanding ice within deformed and breached them)
and as of today, the freeze-boil cycle has produced cracks all over one of my mugs,
rendering it 'art' and useless for coffeeing purposes.

I have a project in mind to put my captive sphere bearing in all of the 'twitch handles',
so let's see what that'll look like, what it'll take to accomplish.
I said putting the bearings in a single design would take a day, and instead it took a month,
soooooooo....

anyways, let's see. First, where are all the twitch handles now?  

We find the designer and objects at https://www.thingiverse.com/michaeltaylorresearch/designs
We also find good fortune in the licensing terms: TH1 - Twitch Handle One by michaeltaylorresearch is licensed under the Creative Commons - Attribution license.
We will, of course, be paying it forward with our freedom terms.
Let's call Michael Taylor and ask about stuff! Maybe make a new friend? He IS one of my people, I can tell.
Upload bearing and remix
---
Long, interesting, productive discussion with this new friend about life, the universe, and everything
I promised to send him my bearing design and remixes, he said he'd send me the
twitch handle source files (fusion360, guess I'll be trying that out)
I also promised MYSELF to release the hybrid twitch handle on Thingiverse. That's TODAY.
So, I sent the email and attachments, now it's time to ready my own release.
Published bearing to https://www.thingiverse.com/thing:3017275 with stl and scad code.
Published TH2-remix to https://www.thingiverse.com/thing:3017281 with stl and pic.
---

I met a girl with a circular gallifreyan tattoo of a Charles Bukowski quote.
It's a puzzle, so I'm finding what the quote is! https://adrian17.github.io/Gallifreyan/ :)
who doesn't love a good puzzle?
I got the word 'gotten' I think, and I should be able to narrow it down.
Tricky.
Looks like part of this:
" “I sit here
drunk now.
I am
a series of
small victories
and large defeats
and I am as
amazed
as any other
that
I have gotten
from there to
here
without committing murder
or being
murdered;
without
having ended up in the
madhouse.

as I drink alone
again tonight
my soul despite all the past
agony
thanks all the gods
who were not
there
for me
then.”

― Charles Bukowski, The People Look Like Flowers at Last "

Since I found "I have gotten from there to here without" in the tattoo, seems I should be pretty confident.
Ah, a mistake - the tattoo ends at the word "[to] here".
Yup! solving puzzles feels rad. Super thanks to this translator! https://adrian17.github.io/Gallifreyan/


---

Well, another day somehow spent. I'll buy no more coffee, and shall transition to caffiene pills
so I can reduce my dose more controlledly -  a nice monotonic decrease to zero.

I threw together 2 (2.5? 3?) new models of Twitch Handles with my gyrobearing.
I'll print 'em tomorrow hopefully! They look good sliced in Cura, so I expect good results.
Goodnight.
