// Sphere Gyro Bearings for Fidgets
// Ryan Petroff's design

// Parts: outer body, inner sphere shell hole,
//        inner sphere shell solid, inner void

// All measurements in mm milimeters 

// Height of whole print
H = 7 ; 

// Minimum gap size on printer (discovered)
G = 0.35 ;

// Outer body - should be drop-in replacement for skateboard bearings
// for reference: 8mm (core hole), 22mm (outer diameter), and 7mm (thickness)
RRRR = (22/2) ;
// cylinder ( r=RRRR, h=H, $fn=100 ) ;

// R values should be derived, but will be set for now

// Radius of spheres: RRR outer body cutout
RRR = RRRR - G - 1; 
// this -1 is experimental

// Radius of spheres: RR inner gyro shell
RR = RRR - G;



//sphere ( R1 , center = true ) ;
//sphere ( R2 , center = true ) ;

// Outer race
difference () {
    cylinder ( r=RRRR, h=H, $fn=100, center=true );
    sphere (RRR, center=true);
} ;

// Inner race
difference () {
    sphere (RR, center=true);
    translate([-50,-50,(H/2)])
        cube ([100,100,100]);
    translate([-50,-50,((-100)+(-H/2))])
        cube ([100,100,100]);
    translate([0,0,-H])
        cylinder ( r=(8/2), h=(2*H), $fn=100 ) ;
}