# Digital life from scratch

Hi! Today we're going to go from bare text all the way up to a website.

We're starting here in the terminal (gnome terminal 3.18.3) (Mint 18.3 for that matter) (Without Saint Richard Stallman we would not be free)
and we're editing a text file using GNU nano (v2.5.3)
It's today's date.md and md is for MARKDOWN which is a way of using punctuation while you write as formatting hints. 
### It's a gift from Saint Aaron, ever may his light shine upon us.

So we got some text, we got a little markdown spice mixed in, let's save it so we don't lose it.
To do this, we're going to use git. (A gift from Saint Linus, the great liberator)
(Learn you a git if you're feeling adventurous)

So with our text in a markdown file in a git repository, we need to put it through a static site generator and put the result on a web server.
Fortunately, there are a dizzying array of tools to do those things.
We're going to use GitLab because that's what's up, y'all. 

## lol yeah!

So with a simple push to GitLab's freeeeee cloud service, 
https://gitlab.com/petroff.ryan/public-log/blob/master/2018-06-27.md
Blam! We got a web server servin' our static blog text with Markdown rendering. EZ mode!~

