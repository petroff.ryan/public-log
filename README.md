# Ryan's Public Log

Ryan's major narrative arc is towards founding a freedom school in Toronto.   
After that, the dream is to free as many children around the world as possible.

Ambitious projects require allies and teamwork, and evangelism.

By developing a writing and publishing habit, Ryan will build up a stock of raw material
from which to distill useful writings and polished content.

Positivity and progress are the watchwords of this project.

By creating valuable content, Ryan will be able to convince minds and gain an audience all over the world.
With friends and supporters all over the world, success becomes much more possible!

